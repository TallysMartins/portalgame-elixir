defmodule PortalTest do
  use ExUnit.Case
  doctest Portal

  setup do
    pid = Process.whereis(:orange)
    unless is_nil(pid) do
      Process.exit(pid, :shutdown)
    end
    :ok
  end

  test "shoot a portal door with a given color" do
    {:ok, pid} = Portal.shoot(:orange)
    door = Process.whereis(:orange)

    assert  pid == door
  end

  test "not shoot a portal door with the same color" do
    {:ok, _} = Portal.shoot(:orange)
    {:error, _} = Portal.shoot(:orange)
  end

  test "portal door `color` must be an atom" do
    {:error, "Color must be an :atom"} = Portal.shoot(nil)
    {:error, "Color must be an :atom"} = Portal.shoot(1)
    {:error, "Color must be an :atom"} = Portal.shoot("string")
  end
end
